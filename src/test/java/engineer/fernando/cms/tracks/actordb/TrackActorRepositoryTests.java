package engineer.fernando.cms.tracks.actordb;

import engineer.fernando.cms.domainmodel.avro.TrackIFFClassificationType;
import engineer.fernando.cms.domainmodel.avro.TrackType;
import engineer.fernando.cms.tracks.actordb.model.TrackActor;
import engineer.fernando.cms.tracks.actordb.model.TrackActorBuilder;
import engineer.fernando.cms.tracks.actordb.repository.TrackActorRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

@ActiveProfiles("test")
@DataJpaTest
 class TrackActorRepositoryTests {

    static TrackActor zolino;
    static TrackActor coisudinha;
    static TrackActor enemy;
    static TrackActor neutral;


    @Autowired
    TrackActorRepository repository;

    @BeforeAll
    static void beforeAll() {

        zolino = new TrackActorBuilder().setMMSI(1979)
                .setFullName("Zolino").setLoa(1.76).setBeam(0.79).setDraught(0.32)
                .setType(TrackType.AMPHIBIOUS).setClassification(TrackIFFClassificationType.FRIEND).build();

        coisudinha = new TrackActorBuilder().setMMSI(1983)
                .setFullName("Coisudinha").setLoa(1.69).setBeam(0.69).setDraught(0.22)
                .setType(TrackType.AIRPLANE).setClassification(TrackIFFClassificationType.FRIEND).build();

        enemy = new TrackActorBuilder().setMMSI(1939)
                .setFullName("EnemyTrack").setType(TrackType.DESTROYER)
                .setClassification(TrackIFFClassificationType.HOSTILE).build();

        neutral = new TrackActorBuilder().setMMSI(1234)
                .setFullName("UnknownTrack").build();
    }

    @BeforeEach
    void setUp() {

    }

    @Test
    void testEmptyRepository() {
        List<TrackActor> aisTracks = repository.findAll();
        assertTrue(aisTracks.isEmpty());
    }

    @Test
    void testRepository() {
        TrackActor trackActor = repository.save(zolino);
        assertEquals(1979,trackActor.getMMSI());
        assertEquals("Zolino",trackActor.getFullName());
        assertEquals(1.76,trackActor.getLoa(),0.01);
        assertEquals(0.79,trackActor.getBeam(),0.01);
        assertEquals(0.32,trackActor.getDraught(),0.01);
        assertEquals(1,repository.count());

        TrackActor anotherTrackActor = repository.save(coisudinha);
        assertEquals(1983,anotherTrackActor.getMMSI());
        assertEquals(TrackType.AIRPLANE,anotherTrackActor.getType());
        assertEquals(TrackIFFClassificationType.FRIEND,anotherTrackActor.getClassification());
        assertEquals(2,repository.count());

        TrackActor enemyTrack = repository.save(enemy);
        assertEquals(1939,enemyTrack.getMMSI());
        assertEquals("EnemyTrack",enemyTrack.getFullName());
        assertEquals(TrackType.DESTROYER,enemyTrack.getType());
        assertEquals(3,repository.count());

        TrackActor neutralTrack = repository.save(neutral);
        assertEquals(1234,neutralTrack.getMMSI());
        assertEquals("UnknownTrack",neutralTrack.getFullName());
        assertEquals(TrackType.UNKNOWN,neutralTrack.getType());
        assertEquals(TrackIFFClassificationType.UNKNOWN,neutralTrack.getClassification());
        assertEquals(4,repository.count());

        //Acquiring neutral track
        Optional<TrackActor> optTrack = repository.findByMmsi(1234);
        assertTrue(optTrack.isPresent());
        assertEquals("UnknownTrack",optTrack.get().getFullName());

        //Acquiring neutral track
        repository.deleteByMmsi(1234);
        // , and remains only 3 actors
        assertEquals(3,repository.count());
    }

}
