package engineer.fernando.cms.tracks.actordb.controller;

import engineer.fernando.cms.domainmodel.avro.Track;
import engineer.fernando.cms.domainmodel.util.Codec;
import engineer.fernando.cms.tracks.actordb.model.TrackActor;
import engineer.fernando.cms.tracks.actordb.model.TrackActorBuilder;
import engineer.fernando.cms.tracks.actordb.repository.TrackActorRepository;
import org.apache.avro.generic.GenericRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
public class TrackActorDatabaseController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TrackActorDatabaseController.class);

    @Autowired
    private TrackActorRepository trackActorRepository;

    @PostMapping(
            value = "/actor",
            consumes = MediaType.APPLICATION_OCTET_STREAM_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<TrackActor>> publishTrack(@RequestBody byte[] data){
        List<TrackActor> receivedActors = new ArrayList<>();
        try {
            List<GenericRecord> records = Codec.deserializeToAvroRecord(data);
            for (GenericRecord avroRecord: records) {
                if(LOGGER.isDebugEnabled()){
                    LOGGER.debug(String.format("Processing record: %s ", avroRecord.toString()));
                }
                if (!(avroRecord instanceof Track)) {
                    if(LOGGER.isErrorEnabled()){
                        LOGGER.error(String.format("Record %s is not instance of Track Schema: %s ", avroRecord.toString(), Track.getClassSchema().toString()));
                    }
                    continue;
                }
                Track receivedActor = (Track) avroRecord;
                if(LOGGER.isDebugEnabled()){
                    LOGGER.debug("Record converted into Track");
                }
                TrackActorBuilder builder = new TrackActorBuilder();
                builder.setMMSI(receivedActor.getTrackId()).setFullName((String) receivedActor.getName())
                        .setLoa(receivedActor.getTrackSpec().getDimension().getLoa())
                        .setBeam(receivedActor.getTrackSpec().getDimension().getBeam())
                        .setDraught(receivedActor.getTrackSpec().getDimension().getDraught())
                        .setClassification(receivedActor.getIffClassification());
                TrackActor lActor = trackActorRepository.save(builder.build());
                receivedActors.add(lActor);
                if(LOGGER.isInfoEnabled()){
                    LOGGER.info(String.format("Received Track Actor: %s",lActor));
                }
            }
            return new ResponseEntity<>(receivedActors, HttpStatus.CREATED);
        }
        catch (Exception e){
            if(LOGGER.isErrorEnabled()){
                LOGGER.error(String.format("Exception: %s ", e));
            }
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/actor/{mmsi}")
    public ResponseEntity<TrackActor> updateTrack(@PathVariable("mmsi") long mmsi, @RequestBody byte[] data) {
        try {
            GenericRecord avroRecord = Codec.deserializeToAvroRecord(data).get(0);
            if (!(avroRecord instanceof Track)) {
                if(LOGGER.isErrorEnabled()){
                    LOGGER.error(String.format("Record %s is not instance of Track Schema: %s ", avroRecord.toString(), Track.getClassSchema().toString()));
                }
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            Track receivedActor = (Track) avroRecord;
            if(LOGGER.isDebugEnabled()){
                LOGGER.debug("Record converted into Track");
            }
            Optional<TrackActor> actorData = trackActorRepository.findByMmsi(mmsi);
            if (actorData.isPresent()) {
                TrackActor lActor = actorData.get();
                lActor.setFullName((String) receivedActor.getName());
                lActor.setLoa(receivedActor.getTrackSpec().getDimension().getLoa());
                lActor.setBeam(receivedActor.getTrackSpec().getDimension().getBeam());
                lActor.setDraught(receivedActor.getTrackSpec().getDimension().getDraught());
                lActor.setType(receivedActor.getTrackSpec().getTrackType());
                lActor.setClassification(receivedActor.getIffClassification());
                return new ResponseEntity<>(trackActorRepository.save(lActor), HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (IOException e) {
            if(LOGGER.isErrorEnabled()){
                LOGGER.error(String.format("Exception: %s ", e));
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/actor")
    public ResponseEntity<List<TrackActor>> getAllTracks() {
        try {
            List<TrackActor> trackActors = new ArrayList<>();
            trackActorRepository.findAll().forEach(trackActors::add);
            if (trackActors.isEmpty() && LOGGER.isInfoEnabled()) {
                LOGGER.info("No actor found in DB");
            }
            return new ResponseEntity<>(trackActors, HttpStatus.OK);
        } catch (Exception e) {
            if(LOGGER.isErrorEnabled()){
                LOGGER.error(String.format("Exception: %s ", e));
            }
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/actor/{mmsi}")
    public ResponseEntity<TrackActor> getTrackByMmsi(@PathVariable("mmsi") long mmsi) {
        Optional<TrackActor> trackActorList = trackActorRepository.findByMmsi(mmsi);
        if (trackActorList.isPresent()) {
            return new ResponseEntity<>(trackActorList.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/actor")
    public ResponseEntity<HttpStatus> deleteAllTracks() {
        try {
           trackActorRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            if(LOGGER.isErrorEnabled()){
                LOGGER.error(String.format("Exception: %s ", e));
            }
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/actor/{mmsi}")
    public ResponseEntity<HttpStatus> deleteTrackByMmsi(@PathVariable("mmsi") long mmsi) {
        try {
            trackActorRepository.deleteByMmsi(mmsi);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            if(LOGGER.isErrorEnabled()){
                LOGGER.error(String.format("Exception: %s ", e));
            }
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
