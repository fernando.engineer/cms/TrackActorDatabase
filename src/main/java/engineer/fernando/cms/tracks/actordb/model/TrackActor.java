package engineer.fernando.cms.tracks.actordb.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import engineer.fernando.cms.domainmodel.avro.TrackIFFClassificationType;
import engineer.fernando.cms.domainmodel.avro.TrackType;

@Entity
@Table(name = "trackactors")
public class TrackActor {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "mmsi")
    private long mmsi;

    @Column(name = "fullName")
    private String fullName;

    @Column(name = "loa")
    private double loa;

    @Column(name = "beam")
    private double beam;

    @Column(name = "draught")
    private double draught;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private TrackType type;

    @Column(name = "classification")
    @Enumerated(EnumType.STRING)
    private TrackIFFClassificationType classification;


    public long getId() {
        return id;
    }

    public long getMMSI() {
        return mmsi;
    }

    public void setMMSI(long mmsi) {
        this.mmsi = mmsi;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public double getLoa() {
        return loa;
    }

    public void setLoa(double loa) {
        this.loa = loa;
    }

    public double getBeam() {
        return beam;
    }

    public void setBeam(double beam) {
        this.beam = beam;
    }

    public double getDraught() {
        return draught;
    }

    public void setDraught(double draught) {
        this.draught = draught;
    }

    public TrackType getType() {
        return type;
    }

    public void setType(TrackType type) {
        this.type = type;
    }

    public TrackIFFClassificationType getClassification() {
        return classification;
    }

    public void setClassification(TrackIFFClassificationType classification) {
        this.classification = classification;
    }

    @Override
    public String toString() {
        return "TrackActor [id=" + id + ", mmsi=" + mmsi + ", fullName=" + fullName + ", " +
                "dim[loa,beam,draught]=" + loa +"x"+beam+"x"+draught+ ", type=" + type + ", classification=" + classification + ", received.]";
    }

}