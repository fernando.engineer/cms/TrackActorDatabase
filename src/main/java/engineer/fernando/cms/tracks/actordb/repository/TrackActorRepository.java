package engineer.fernando.cms.tracks.actordb.repository;


import engineer.fernando.cms.tracks.actordb.model.TrackActor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface TrackActorRepository extends JpaRepository<TrackActor,Long> {

    Optional<TrackActor> findByMmsi(long mmsi);

    @Transactional
    @Modifying
    @Query("DELETE FROM TrackActor c WHERE c.mmsi = :mmsi")
    void deleteByMmsi(long mmsi);
}
