package engineer.fernando.cms.tracks.actordb.model;

import engineer.fernando.cms.domainmodel.avro.TrackIFFClassificationType;
import engineer.fernando.cms.domainmodel.avro.TrackType;

public class TrackActorBuilder {

    private long mmsi;
    private String fullName;
    private double loa;
    private double beam;
    private double draught;

    private TrackType type = TrackType.UNKNOWN;
    private TrackIFFClassificationType classification = TrackIFFClassificationType.UNKNOWN;

    public TrackActorBuilder setMMSI(long mmsi) {
        this.mmsi = mmsi;
        return this;
    }

    public TrackActorBuilder setFullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    public TrackActorBuilder setLoa(double loa) {
        this.loa = loa;
        return this;
    }

    public TrackActorBuilder setBeam(double beam) {
        this.beam = beam;
        return this;
    }

    public TrackActorBuilder setDraught(double draught) {
        this.draught = draught;
        return this;
    }

    public TrackActorBuilder setType(TrackType type) {
        this.type = type;
        return this;
    }

    public TrackActorBuilder setClassification(TrackIFFClassificationType classification) {
        this.classification = classification;
        return this;
    }

    public TrackActor build(){
        TrackActor track = new TrackActor();
        track.setMMSI(this.mmsi);
        track.setFullName(this.fullName);
        track.setLoa(this.loa);
        track.setBeam(this.beam);
        track.setDraught(this.draught);
        track.setType(this.type);
        track.setClassification(this.classification);
        return track;
    }

}
